package exception;
/**
 * Exception concernant la longueur d'un bateau
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class LongueurException extends BateauException {

	/**
	 * Constructeur de LongueurException.java pour un message donn�
	 * @param message Message d'erreur de l'exception
	 */
	public LongueurException(String message) {
		super(message);
	}

}
