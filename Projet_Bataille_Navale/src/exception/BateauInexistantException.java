package exception;
/**
 * Exception concernant un bateau inexistant
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class BateauInexistantException extends Exception {

	/**
	 * Constructeur de BateauInexistantException.java pour un message donn�
	 * @param message Message d'erreur de l'exception
	 */
	public BateauInexistantException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
}
