package exception;
/**
 * Exception concernant les bateaux
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class BateauException extends Exception {

	/**
	 * Constructeur de BateauException.java pour un message donn�
	 * @param message Message d'erreur de l'exception
	 */
	public BateauException(String message) {
		super(message);
	}
}
