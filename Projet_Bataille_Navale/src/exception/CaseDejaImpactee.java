/**
 * Exception lorsque la case a d�j� �t� impact�e par un tir
 */
package exception;

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class CaseDejaImpactee extends GrilleException {

	/**
	 * Constructeur de CaseDejaImpactee.java pour un message donn�
	 * @param message Message d'erreur de l'exception
	 */
	public CaseDejaImpactee(String message) {
		super(message);
	}

}
