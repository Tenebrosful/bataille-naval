package jeu;
/**
 * Mod�lisation d'un bateau
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import exception.LongueurException;

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class Bateau implements Serializable, Comparable<Bateau>{

	/**
	 * Liste des noms par d�faut des bateaux initialis� � partir de noms de
	 * https://fr.wikipedia.org/wiki/Liste_de_bateaux_imaginaires
	 */
	private static List<String> listeNomBateauDefaut = new ArrayList<String>(Arrays.asList("Arche de No�", "Argo", "Nagifar",
			"Hollandais Volant", "Caleuche", "Princess Augusta", "Yarmouth", "Nautilus", "Forward", "Duncan",
			"Franklin"));

	/**
	 * Ajoute un nom de bateau � la liste par d�faut
	 * 
	 * @param nomBateau Nom du bateau � ajouter
	 */
	public static void addListeNomBateauDefaut(String nomBateau) {
		Bateau.listeNomBateauDefaut.add(nomBateau);
	}

	/**
	 * Retourne listeNomBateauDefaut
	 * 
	 * @return listeNomBateauDefaut
	 */
	public static List<String> getListeNomBateauDefaut() {
		return listeNomBateauDefaut;
	}

	/**
	 * Retire un nom de bateau � la liste par d�faut
	 * 
	 * @param nomBateau Nom du bateau � ajouter
	 */
	public static void removeListeNomBateauDefaut(String nomBateau) {
		Bateau.listeNomBateauDefaut.remove(nomBateau);
	}

	/**
	 * Permet de red�finir la liste enti�re des noms de bateau par d�faut
	 * 
	 * @param listeNomBateauDefaut Nouvelle valeur de listeNomBateauDefaut
	 */
	public static void setListeNomBateauDefaut(List<String> listeNomBateauDefaut) {
		Bateau.listeNomBateauDefaut = listeNomBateauDefaut;
	}

	/**
	 * Constructeur de Bateau.java pour un longueur et un nom donn�s
	 * 
	 * @param longueur Longueur du bateau
	 * @param nom      Nom du bateau
	 * @throws LongueurException Lanc�e lorsque le bateau a une taille n�gative
	 */
	public Bateau(int longueur, String nom) throws LongueurException {
		if (longueur <= 0)
			throw new LongueurException("La taille d'un bateau ne peut pas �tre n�gative ou nulle (" + longueur + ")");
		else
			this.longueur = longueur;

		if (nom == null || nom.isEmpty()) {
			int numNomBateau = (int) (Math.rint(Math.random() * listeNomBateauDefaut.size()));
			this.nom = listeNomBateauDefaut.get(numNomBateau);
		} else
			this.nom = nom;
	}

	/**
	 * Position en x de l'origine du bateau
	 */
	private int posX;

	/**
	 * Position en y de l'origine du bateau
	 */
	private int posY;

	/**
	 * Longueur du bateau en nombre de case
	 */
	private int longueur;

	/**
	 * Orientation du bateau par rapport � son origine
	 */
	private String orientation;

	/**
	 * Nom du bateau
	 */
	private String nom;

	@Override
	public int compareTo(Bateau arg0) {
		return arg0.getLongueur()-this.getLongueur();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bateau other = (Bateau) obj;
		if (longueur != other.longueur)
			return false;
		if (!nom.equals(other.nom))
			return false;
		if (orientation == null) {
			if (other.orientation != null)
				return false;
		} else if (!orientation.equals(other.orientation))
			return false;
		if (posX != other.posX)
			return false;
		if (posY != other.posY)
			return false;
		return true;
	}

	/**
	 * Retourne longueur
	 * 
	 * @return longueur
	 */
	public int getLongueur() {
		return longueur;
	}

	/**
	 * Retourne nom
	 * 
	 * @return nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Retourne orientation
	 * 
	 * @return orientation
	 */
	public String getOrientation() {
		return orientation;
	}

	/**
	 * Retourne posX
	 * 
	 * @return posX
	 */
	public int getPosX() {
		return posX;
	}

	/**
	 * Retourne posY
	 * 
	 * @return posY
	 */
	public int getPosY() {
		return posY;
	}

	/**
	 * @param orientation Nouvelle valeur de orientation
	 */
	public void setOrientation(String orientation) {
		this.orientation = orientation;
	}

	/**
	 * @param posX Nouvelle valeur de posX
	 * @param posY Nouvelle valeur de posY
	 */
	public void setPos(int posX, int posY) {
		this.posX = posX;
		this.posY = posY;
	}
	
	/**
	 * @param posX Nouvelle valeur de posX
	 */
	public void setPosX(int posX) {
		this.posX = posX;
	}

	/**
	 * @param posY Nouvelle valeur de posY
	 */
	public void setPosY(int posY) {
		this.posY = posY;
	}

	@Override
	public String toString() {
		return nom + " de longueur " + longueur;
	}
}
