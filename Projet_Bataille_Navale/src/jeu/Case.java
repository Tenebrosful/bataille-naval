package jeu;
/**
 * Modèlise une case du jeu
 */

import java.io.Serializable;

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class Case implements Serializable{

	/**
	 * Constructeur de Case.java pour une position (x;y) donnée
	 * 
	 * @param posX Position en x de la case
	 * @param posY Position en y de la case
	 */
	public Case(int posX, int posY) {
		this.posX = posX;
		this.posY = posY;
	}

	/**
	 * Position en x de la case
	 */
	private int posX;

	/**
	 * Position en y de la case
	 */
	private int posY;

	/**
	 * Bateau présent sur la case
	 */
	private Bateau bateau;

	/**
	 * Indique si la case a été déjà ciblé par un tir
	 */
	private boolean impact;

	/**
	 * Permet d'afficher la case de la perspective d'un adversaire V = Bateau touché
	 * X = Case touchée sans bateau
	 * 
	 * @return Modélisation de la case du point de vue d'un adversaire
	 */
	public String afficherAdversaire() {
		if (this.isOccupee() && this.isImpact())
			return "[V]";
		else if (this.isImpact())
			return "[X]";
		else
			return "[ ]";
	}

	/**
	 * Permet d'afficher la case de la perspective du propriétaire de la grille
	 * (avec bateaux apparents) O = Bateau touché B = Bateau
	 * 
	 * @return Modélisation de la case
	 */
	public String afficherProprietaire() {
		if (this.isOccupee() && this.isImpact())
			return "[O]";
		else if (this.isOccupee())
			return "[B]";
		else
			return "[ ]";
	}

	/**
	 * Teste si les coordonnées en paramètre correspondent à la case
	 * 
	 * @param posX Position en x à tester
	 * @param posY Position en y à tester
	 * @return True si la case correspond aux coordonnées en paramètre
	 */
	public boolean aPourCoordonnee(int posX, int posY) {
		return (this.posX == posX && this.posY == posY);
	}

	/**
	 * Retourne bateau
	 * 
	 * @return bateau
	 */
	public Bateau getBateau() {
		return bateau;
	}

	/**
	 * Retourne posX
	 * 
	 * @return posX
	 */
	public int getPosX() {
		return posX;
	}

	/**
	 * Retourne posY
	 * 
	 * @return posY
	 */
	public int getPosY() {
		return posY;
	}

	/**
	 * Retourne impact
	 * 
	 * @return impact
	 */
	public boolean isImpact() {
		return impact;
	}

	/**
	 * Teste si la case est occupée par un bateau
	 * 
	 * @return True si la case est occupée par un bateau
	 */
	public boolean isOccupee() {
		return this.bateau != null;
	}

	/**
	 * Modifie le bateau présent sur la case
	 * 
	 * @param bateau Nouvelle valeur de bateau
	 */
	public void setBateau(Bateau bateau) {
		this.bateau = bateau;
	}

	/**
	 * Modifie l'état d'impact du bateau présent sur la case
	 * 
	 * @param impact Nouvelle valeur de impact
	 */
	public void setImpact(boolean impact) {
		this.impact = impact;
	}

	@Override
	public String toString() {
		return "[" + this.getPosX() + ";" + this.getPosY() + ";" + this.isOccupee() + ";" + this.isImpact() + "]";
	}

}
