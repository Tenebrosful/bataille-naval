package jeu;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import exception.*;

/**
 * Programme principal g�rant le d�roulement du jeu
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class Principal {

	private static Scanner sc = new Scanner(System.in);
	private static Joueur j1;
	private static Joueur j2;

	/**
	 * Correspond � un type de partie 0 = Aucune (Actuellement en menu) 1 = Partie
	 * Mono-joueur 2 = Partie Multi-joueur
	 */
	private static int codePartie = 0;

	/**
	 * Permet de charge une partie � partir d'un fichier de sauvegarde
	 * 
	 * @param nomF Nom du fichier de sauvegarde � charger
	 * @throws IOException            Lanc�e lors d'une erreur durant la lecture du
	 *                                fichier � charger
	 * @throws FileNotFoundException  Lanc�e lorsque le fichier nomF.sav n'a pas �t�
	 *                                trouv�
	 * @throws ClassNotFoundException Lanc�e lorsque l'objet lu n'est pas du type
	 *                                Joueur
	 */
	public static void chargerPartie(String nomF) throws FileNotFoundException, IOException, ClassNotFoundException {
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(nomF + ".sav"));

		codePartie = ois.readInt();
		j1 = (Joueur) ois.readObject();
		j2 = (Joueur) ois.readObject();

		ois.close();
	}

	/**
	 * Mode de jeu � 1 joueur
	 */
	public static void jeuMonoJoueur() {

		System.out.println("\n\n\n============Jeu � un joueur============\n");
		int nbrTir = 0;
		boolean continuer = true;
		while (j1.getGrille().avoirBateauVivant() && continuer) {
			System.out.println("\n\n\n" + j1.getGrille().afficherAdversaire());

			System.out.println("\n   Liste des bateaux : ");
			j1.trierListeBateau();
			for (Bateau tmp : j1.getListeBateau()) {
				System.out.println(tmp + " (" + (j1.getGrille().etatBateau(tmp) * 100) / tmp.getLongueur() + "%)");
			}

			boolean entreeValide = false;

			while (!entreeValide) {
				System.out.print("\n Que voulez-vous faire ?");
				System.out.print("     1) Tirer");
				System.out.print("     2) Sauvegarder");
				System.out.print("     3) Menu Principal");
				System.out.print("\nNum�ro de menu : ");

				int choix = 0;
				try {
					choix = sc.nextInt();
					sc.nextLine(); // Passage � la prochaine ligne
				} catch (Exception e) {
					sc.nextLine();
				}

				switch (choix) {
				case 1:
					while (!entreeValide) {
						System.out.print("\n     Veuillez indiquer la position en x du tir : ");
						int x = -1;
						try {
							x = sc.nextInt();
							sc.nextLine(); // Passage � la prochaine ligne
						} catch (Exception e) {
							sc.nextLine();
						}

						System.out.print("     Veuillez indiquer la position en y du tir : ");
						int y = -1;
						try {
							y = sc.nextInt();
							sc.nextLine(); // Passage � la prochaine ligne
						} catch (Exception e) {
							sc.nextLine();
						}

						try {
							j1.tirer(j1, x, y);
							nbrTir++;
							entreeValide = true;
						} catch (PositionException e) {
							System.out.println("\n   /!\\ La position est invalide, veuillez r��ssayez /!\\\n");
						} catch (CaseInexistanteException e) {
							System.out.println("\n   /!\\ La case n'existe pas, veuillez r��ssayez /!\\\n");
						} catch (CaseDejaImpactee e) {
							System.out.println("\n   /!\\ La case est d�j� impact�e, veuillez r��ssayez /!\\\n");
						}
					}
					break;

				case 2:
					System.out.print("\n     Veuillez indiquer le nom du nouveau fichier : ");
					try {
						sauvegarderPartie(sc.nextLine());
						System.out.println("\n     Sauvargarde effectu�e !");
					} catch (IOException e) {
						System.out.println("\n   /!\\ Un erreur s'est produite /!\\\n");
						e.printStackTrace();
					}
					break;

				case 3:
					System.out.print(
							"\n     Attention toutes modifications non sauvegard�e sera perdue �tes-vous s�r de vouloir quitter ? (y/n) : ");
					if (sc.nextLine().equals("y")) {
						continuer = false;
						entreeValide = true;
						System.out.println();
					}

					break;

				default:
					System.out.println("\n   /!\\ La r�ponse est invalide, veuillez r��ssayez /!\\\n");
					break;

				}
			}
		}

		if (!j1.getGrille().avoirBateauVivant()) {
			System.out.println("\n\n\n====================================" + "\n===================================="
					+ "\n==============VICTOIRE==============" + "\n===================================="
					+ "\n====================================");
			System.out.println(" Vous avez gagn� en " + nbrTir + " tirs !");

			System.out.println("\nAppuyez sur Entrer pour retourner au menu principal");
			sc.nextLine(); // Permet de mettre en pause
		}
	}

	/**
	 * Mode de jeu � plusieurs joueurs
	 */
	public static void jeuMultiJoueur() {
		codePartie = 2;
		System.out.println("Cette fonctionabilit� n'a pas encore �t� implent�");
		sc.nextLine();
	}

	/**
	 * Permet de lancer une parte � partir du code de partie actuel
	 */
	public static void lancerAvecCodePartie() {
		switch (codePartie) {
		case 1:
			jeuMonoJoueur();
			break;
		case 2:
			jeuMultiJoueur();
			break;
		}
	}

	/**
	 * M�thode principal g�rant le d�roulement du jeu
	 * 
	 * @param args (initulis�)
	 * @throws TailleException          Lanc�e lorsque la taille de la grille est
	 *                                  n�gative
	 * @throws LongueurException        lanc�e lorsque la longueur d'un bateau est
	 *                                  n�gatif
	 * @throws ListeBateauVideException Lanc�e lorsque la liste de bateau est vide
	 * @throws OrientationException     Lanc�e lorsque l'orientation n'est pas
	 *                                  valide
	 * @throws CaseOccupeeException     lanc�e lorsqu'une case est d�j� occup�e
	 * @throws CaseInexistanteException lanc�e lorsque la case n'existe pas
	 */
	public static void main(String[] args) throws TailleException, LongueurException, ListeBateauVideException,
			CaseInexistanteException, CaseOccupeeException, OrientationException {

		boolean menu = true;
		while (menu) {
			System.out.println("============\n" + "Bataille Navale\n" + "============");
			System.out.println("Menu principal");
			System.out.println("     1) Jeu mono-joueur");
			System.out.println("     2) Jeu � 2 joueurs");
			System.out.println("     3) Liste de noms des bateaux par d�faut");
			System.out.println("     4) Quitter le jeu");
			System.out.print("\nNum�ro de menu : ");

			int choix = 0;
			try {
				choix = sc.nextInt();
				sc.nextLine(); // Passage � la prochaine ligne
			} catch (Exception e) {
				sc.nextLine();
			}

			switch (choix) {
			case 1:
				menuJeuMonoJoueur();
				break;

			case 2:
				jeuMultiJoueur();
				break;

			case 3:
				menuNomBateauDefaut();
				break;

			case 4:
				menu = false;
				break;

			default:
				System.out.println("\n   /!\\ La r�ponse est invalide, veuillez r��ssayez /!\\\n");
				break;

			}

			codePartie = 0;
		}

	}

	/**
	 * Menu du mode de jeu � un joueur
	 */
	public static void menuJeuMonoJoueur() {
		codePartie = 1;
		boolean menu = true;
		while (menu) {
			System.out.println("\n\n\n============Mode 1 joueur============\n");
			System.out.println("     1) Nouvelle partie");
			System.out.println("     2) Charger une partie");
			System.out.println("     3) Retour");
			System.out.print("\nNum�ro de menu : ");

			int choix = 0;
			try {
				choix = sc.nextInt();
				sc.nextLine(); // Passage � la prochaine ligne
			} catch (Exception e) {
				sc.nextLine();
			}

			switch (choix) {
			case 1:
				if (setupMonoJoueur()) {
					jeuMonoJoueur();
					menu = false;
				}
				break;

			case 2:
				System.out.print("\n\n Veuillez entrer le nom du fichier .sav : ");
				String nomF = sc.nextLine();

				try {
					chargerPartie(nomF);
					if (codePartie == 1) {
						jeuMonoJoueur();
						menu = false;
					} else {
						System.out.println("\\n   /!\\\\ Le fichier ne correspond pas � ce type de parti (" + codePartie
								+ "), voulez-vous la charger quand m�me ? (y/n) : ");
						if (sc.nextLine().equals("y")) {
							lancerAvecCodePartie();
							menu = false;
						}

					}
				} catch (FileNotFoundException e) {
					System.out.println("\n   /!\\ Le fichier est introuvable, veuillez r��ssayez /!\\\n");
				} catch (ClassNotFoundException | IOException e) {
					System.out.println("\n   /!\\ Le fichier est incorrect ou corrompu, veuillez r��ssayez /!\\\n");
				}
				break;

			case 3:
				menu = false;
				break;

			default:
				System.out.println("\n   /!\\ La r�ponse est invalide, veuillez r��ssayez /!\\\n");
				break;
			}
		}
	}

	/**
	 * Menu permettant de gerer les noms des bateaux par d�faut
	 */
	private static void menuNomBateauDefaut() {
		boolean menu = false;
		while (!menu) {
			System.out.println("\n\n\n============Noms de bateau par d�faut============\n");

			System.out.println("   Liste actuelle : ");
			for (String tmp : Bateau.getListeNomBateauDefaut()) {
				System.out.println("     - " + tmp);
			}

			System.out.print("\n Que voulez-vous faire ?");
			System.out.print("     1) Ajouter un nom");
			System.out.print("     2) Retirer un nom");
			System.out.print("     3) Remplacer par une liste");
			System.out.print("     4) Retour");
			System.out.print("\nNum�ro de menu : ");

			int choix = 0;
			try {
				choix = sc.nextInt();
				sc.nextLine(); // Passage � la prochaine ligne
			} catch (Exception e) {
				sc.nextLine();
			}

			switch (choix) {
			case 1:
				System.out.print("\n     Veuillez indiquer le nom � rajouter : ");
				Bateau.addListeNomBateauDefaut(sc.nextLine());
				break;

			case 2:
				System.out.print("\n     Veuillez indiquer le nom � retirer : ");
				Bateau.removeListeNomBateauDefaut(sc.nextLine());
				break;

			case 3:
				System.out.print("\n     Nombre de nom : ");
				int nbrNom = 0;
				try {
					nbrNom = sc.nextInt();
					sc.nextLine(); // Passage � la prochaine ligne
				} catch (Exception e) {
					sc.nextLine();
				}

				List<String> tmp = new ArrayList<String>();
				for (int i = 0; i < nbrNom; ++i) {
					System.out.print("\n     Veuillez indiquer le nom n�" + i + " : ");
					tmp.add(sc.nextLine());
				}

				Bateau.setListeNomBateauDefaut(tmp);
				break;

			case 4:
				menu = false;

			}
		}
	}

	/**
	 * Permet de sauvegarder la partie en cours
	 * 
	 * @param nomF Nom du fichier de sauvegarde
	 * @throws IOException Lanc�e lors d'une erreur durant l'�criture du fichier de
	 *                     sauvegarde
	 */
	public static void sauvegarderPartie(String nomF) throws IOException {
		if (nomF == null || nomF.isEmpty() || nomF.equals("nomBateauDefaut"))
			nomF = "save" + (int) Math.rint(Math.random() * 99);

		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(nomF + ".sav"));

		oos.writeInt(codePartie);
		oos.writeObject(j1);
		oos.writeObject(j2);

		oos.close();
	}

	/**
	 * Param�trage d'une partie � un seul joueur
	 * 
	 * @return True si la setup c'est correctement pass�
	 */
	public static boolean setupMonoJoueur() {
		System.out.println("\n\n\n============Cr�ation d'une partie 1 joueur============\n");

		Grille grille = null;

		boolean entreeValide = false;
		while (!entreeValide) {
			System.out.println(
					" Note : Pour des raisons de lisibilit� il est d�conseill� de creer une grille plus grande que 50x50 en fonction de la taille et la r�solution de la fen�te");
			System.out.print("\n     veuillez indiquer la taille de la grille en x : ");

			int x = 0;
			try {
				x = sc.nextInt();
				sc.nextLine(); // Passage � la prochaine ligne
			} catch (Exception e) {
				sc.nextLine();
			}

			System.out.print("\n     veuillez indiquer la taille de la grille en y : ");

			int y = 0;
			try {
				y = sc.nextInt();
				sc.nextLine(); // Passage � la prochaine ligne
			} catch (Exception e) {
				sc.nextLine();
			}

			try {
				grille = new Grille(x, y);
				entreeValide = true;
			} catch (TailleException e) {
				System.out.println("\n   /!\\ La taille de la grille est invalide, veuillez r�essayer /!\\\n");
			}
		}

		int nbrBateau = 0;

		while (nbrBateau <= 0) {
			System.out.print("\n     Nombre de bateau : ");
			try {
				nbrBateau = sc.nextInt();
				sc.nextLine(); // Passage � la prochaine ligne
			} catch (Exception e) {
				sc.nextLine();
			}

			if (nbrBateau <= 0)
				System.out.println("\n   /!\\ Le nombre de bateau est incorrect, veuillez r�essayer /!\\\n");
		}

		List<Bateau> listeBateauDepart = new ArrayList<Bateau>();

		for (int i = 1; i <= nbrBateau; ++i) {

			entreeValide = false;

			while (!entreeValide) {

				int longueur = 0;
				String nomBat = null;

				System.out.print("\n     Veuillez indiquer la longueur du bateau n�" + i + " : ");

				try {
					longueur = sc.nextInt();
					sc.nextLine(); // Passage � la prochaine ligne
				} catch (Exception e) {
					sc.nextLine();
				}

				System.out.print("\n     Veuillez indiquer le nom du bateau n�" + i
						+ " (ou faites Entrer pour en choisir un al�atoire) : ");
				nomBat = sc.nextLine();

				try {
					if (longueur > grille.getTailleX() && longueur > grille.getTailleY())
						throw new LongueurException("La taille du bateau d�passe la taille de la grille");
					listeBateauDepart.add(new Bateau(longueur, nomBat));
					if (nomBat.isEmpty())
						System.out.println("       Nom al�atoire choisi : " + listeBateauDepart.get(i - 1).getNom());
					entreeValide = true;
				} catch (LongueurException e) {
					System.out.println("\n   /!\\ La longueur du bateau est incorrecte, veuillez r�essayer /!\\\n");
				}
			}
		}

		System.out.println(
				"\n     Veuillez indiquer le nom du joueur (ou faites Entrer pour en choisir un par d�faut) : ");
		String nomJoueur = sc.nextLine();
		try {
			j1 = new Joueur(nomJoueur, grille, listeBateauDepart);
			if (nomJoueur == null || nomJoueur.isEmpty())
				System.out.println("       Nom al�atoire choisi : " + j1.getNomJoueur());
		} catch (ListeBateauVideException e) {
			System.out.println("\n   /!\\ La liste de bateau est vide, retour au menu pr�c�dent /!\\\n");
			return false;
		}

		System.out.println("\n============Placement des bateaux============\n");

		j1.trierListeBateau();

		for (Bateau tmp : j1.getListeBateau()) {
			entreeValide = false;
			while (!entreeValide) {
				int x = 0, y = 0;
				String orientation = null;

				System.out.println("\n" + j1.getGrille().afficherProprietaire());

				System.out.print("\n     Veuillez entrez la position en x de l'origine du bateau '" + tmp.getNom()
						+ "' de longueur " + tmp.getLongueur() + " : ");
				try {
					x = sc.nextInt();
					sc.nextLine(); // Passage � la prochaine ligne
				} catch (Exception e) {
					sc.nextLine();
				}

				System.out.print("\n     Veuillez entrez la position en y de l'origine du bateau '" + tmp.getNom()
						+ "' de longueur " + tmp.getLongueur() + " : ");
				try {
					y = sc.nextInt();
					sc.nextLine(); // Passage � la prochaine ligne
				} catch (Exception e) {
					sc.nextLine();
				}

				System.out.print("\n     Veuillez entrez l'orientation (Haut,Bas,Gauche,Droite) du bateau '"
						+ tmp.getNom() + "' de longueur " + tmp.getLongueur() + " : ");
				orientation = sc.nextLine();

				try {
					j1.placerBateau(tmp, x, x, orientation);
					entreeValide = true;
				} catch (CaseInexistanteException e) {
					System.out
							.println("\n   /!\\ La case (" + x + ";" + y + ") n'existe pas, veuillez r�essayer /!\\\n");
				} catch (CaseOccupeeException e) {
					System.out.println("\n   /!\\ L'emplacement du bateau est occup�, veuillez r�essayer /!\\\n");
				} catch (OrientationException e) {
					System.out.println("\n   /!\\ L'orientation rentr�e est erron�e, veuillez r�essayer /!\\\n");
				} catch (PositionException e) {
					System.out.println(
							"\n   /!\\ L'orientation combin�e � la position de d�part d�passe de la grille, veuillez r�essayer /!\\\n");
				}
			}
		}
		System.out.println(j1.getGrille().afficherProprietaire());
		return true;
	}
}
