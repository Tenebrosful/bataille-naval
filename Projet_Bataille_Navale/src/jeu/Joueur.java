package jeu;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import exception.CaseDejaImpactee;
import exception.CaseInexistanteException;
import exception.CaseOccupeeException;
import exception.ListeBateauVideException;
import exception.OrientationException;
import exception.PositionException;

/**
 * Mod�lise un joueur
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class Joueur implements Serializable {

	/**
	 * Constructeur de Joueur.java pour un nom, une grille et une liste de Bateau
	 * donn�s
	 * 
	 * @param nomJoueur   Nom du joueur, �gal � "JeanMarDuXX" o� XX est un entier
	 *                    al�atoire entre 0 et 99
	 * @param grille      Grille de jeu appartenant au joueur
	 * @param listeBateau Liste des bateaux de la partie appartenant au joueur
	 * @throws ListeBateauVideException Lanc�e lorsque la liste des bateaux plac� en
	 *                                  param�tre est vide
	 */
	public Joueur(String nomJoueur, Grille grille, List<Bateau> listeBateau) throws ListeBateauVideException {
		if (listeBateau.isEmpty())
			throw new ListeBateauVideException("La liste de bateau plac�e en param�tre est vide");

		if (nomJoueur == null || nomJoueur.isEmpty()) {
			this.nomJoueur = ("JeanMarcDu" + (int) Math.rint(Math.random() * 99));
		} else {
			this.nomJoueur = nomJoueur;
		}

		this.grille = grille;
		this.listeBateau = listeBateau;
	}

	/**
	 * Nom du joueur
	 */
	private String nomJoueur;

	/**
	 * Grille du joueur
	 */
	private Grille grille;

	/**
	 * Liste des bateaux du joueur
	 */
	private List<Bateau> listeBateau;

	/**
	 * Retourne grille
	 * 
	 * @return grille
	 */
	public Grille getGrille() {
		return grille;
	}

	/**
	 * Retourne listeBateau
	 * 
	 * @return listeBateau
	 */
	public List<Bateau> getListeBateau() {
		return listeBateau;
	}

	/**
	 * Retourne nomJoueur
	 * 
	 * @return nomJoueur
	 */
	public String getNomJoueur() {
		return nomJoueur;
	}

	/**
	 * Permet de placer un bateau d'un joueur � une coordonn�e donn�e avec une
	 * orientation donn�e
	 * 
	 * @param orientation Orientation du bateau ("Droite","Gauche","Haut","Bas")
	 * @param bateau      Bateau � placer
	 * @param posX        Future position du bateau en X
	 * @param posY        Future position du bateau en Y
	 * @throws CaseInexistanteException Dans le cas o� il n'existe pas de case aux
	 *                                  coordonn�es entr�es
	 * @throws CaseOccupeeException     Lanc�e lorsqu'il exisite d�j� un bateau dans
	 *                                  la zone d'insertion
	 * @throws OrientationException     Lanc�e lorsque l'orientation du bateau est
	 *                                  �rron�e
	 * @throws PositionException        Lanc�e lorsque la position combin� �
	 *                                  l'orientation d�passe de la grille
	 */
	public void placerBateau(Bateau bateau, int posX, int posY, String orientation)
			throws CaseInexistanteException, CaseOccupeeException, OrientationException, PositionException {
		switch (orientation) {
		case "Droite":
			if (posX + bateau.getLongueur() > this.getGrille().getTailleX()) {
				throw new PositionException(
						"La position (" + posX + ";" + posY + ") ne correspond pas � l'orientation (d�passement)");
			} else {
				for (int i = posX; i < posX + bateau.getLongueur(); ++i) {
					Case tmp = this.grille.getCase(posX, posY);
					if (this.grille.getCase(posX, posY).getBateau() != null) {
						throw new CaseOccupeeException(
								"La case (" + tmp.getPosX() + ";" + tmp.getPosY() + ") est occup�e");
					}
				}
				for (int i = posX; i < posX + bateau.getLongueur(); ++i) {
					this.grille.setBateauCase(bateau, i, posY);
				}
			}
			break;

		case "Gauche":
			if (posX - bateau.getLongueur() + 1 < 0) {
				throw new PositionException(
						"La position (" + posX + ";" + posY + ") ne correspond pas � l'orientation (d�passement)");
			} else {
				for (int i = posX; i > posX - bateau.getLongueur(); --i) {
					Case tmp = this.grille.getCase(posX, posY);
					if (this.grille.getCase(posX, posY).getBateau() != null) {
						throw new CaseOccupeeException(
								"La case (" + tmp.getPosX() + ";" + tmp.getPosY() + ") est occup�e");
					}
				}
				for (int i = posX; i > posX - bateau.getLongueur(); --i) {
					this.grille.setBateauCase(bateau, i, posY);
				}
			}
			break;

		case "Bas":
			if (posY + bateau.getLongueur() > this.getGrille().getTailleY()) {
				throw new PositionException(
						"La position (" + posX + ";" + posY + ") ne correspond pas � l'orientation (d�passement)");
			} else {
				for (int i = posY; i < posY + bateau.getLongueur(); ++i) {
					Case tmp = this.grille.getCase(posX, posY);
					if (this.grille.getCase(posX, posY).getBateau() != null) {
						throw new CaseOccupeeException(
								"La case (" + tmp.getPosX() + ";" + tmp.getPosY() + ") est occup�e");
					}
				}
				for (int i = posY; i < posY + bateau.getLongueur(); ++i) {
					this.grille.setBateauCase(bateau, posX, i);
				}
			}
			break;

		case "Haut":
			if (posY - bateau.getLongueur() + 1 < 0) {
				throw new PositionException(
						"La position (" + posX + ";" + posY + ") ne correspond pas � l'orientation (d�passement)");
			} else {
				for (int i = posY; i > posY - bateau.getLongueur(); --i) {
					Case tmp = this.grille.getCase(posX, posY);
					System.out.println(tmp);
					if (this.grille.getCase(posX, posY).getBateau() != null) {
						throw new CaseOccupeeException(
								"La case (" + tmp.getPosX() + ";" + tmp.getPosY() + ") est occup�e");
					}
				}
				for (int i = posY; i > posY - bateau.getLongueur(); --i) {
					this.grille.setBateauCase(bateau, posX, i);
				}
			}

			break;

		default:
			throw new OrientationException(orientation + " n'est pas une orientation valide");
		}
		bateau.setPosX(posX);
		bateau.setPosY(posY);
		bateau.setOrientation(orientation);
	}

	/**
	 * Permet au joueur de tirer sur la grille d'un autre joueur
	 * 
	 * @param ennemi Joueur cibl� par le tir
	 * @param posX   Position en x de la case cibl�e par le tire
	 * @param posY   Position en y de la case cibl�e par le tire
	 * @throws PositionException        Lanc�e lorsque que la position (x;y) est
	 *                                  invalide
	 * @throws CaseInexistanteException Lanc�e lorsque la case correspondant � la
	 *                                  position (x;y) n'existe pas
	 * @throws CaseDejaImpactee         Lanc�e lorsque la case a d�j� �t� impact�e
	 */
	public void tirer(Joueur ennemi, int posX, int posY)
			throws PositionException, CaseInexistanteException, CaseDejaImpactee {
		if (!ennemi.grille.positionValide(posX, posY))
			throw new PositionException("La position (" + posX + ";" + posY + ") est invalide");
		else if (!ennemi.grille.getCase(posX, posY).isImpact())
			ennemi.grille.getCase(posX, posY).setImpact(true);
		else
			throw new CaseDejaImpactee(" La case (" + posX + ";" + posY + " est d�j� impact�e");
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Joueur [nomJoueur=");
		builder.append(nomJoueur);
		builder.append(", grille=\n");
		builder.append(grille);
		builder.append("\nlisteBateau=\n");
		for (Bateau tmp : this.listeBateau) {
			builder.append(tmp);
			int impact;
			impact = this.grille.etatBateau(tmp);
			builder.append(";" + impact + "(" + (impact * 100) / tmp.getLongueur() + "\n");
		}
		return builder.toString();
	}

	/**
	 * Permet de trier la liste des bateaux du joueurs en fonction de leur taille et
	 * de leur pourcentage de d�g�ts
	 */
	public void trierListeBateau() {
		Collections.sort(this.listeBateau);

		boolean trier = true;
		while (trier) {
			boolean trie = true;
			for (int i = 0; i < this.listeBateau.size() - 1; ++i) {
				if (this.listeBateau.get(i).compareTo(this.listeBateau.get(i + 1)) == 0 && this.grille
						.etatBateau(this.listeBateau.get(i)) < this.grille.etatBateau(this.listeBateau.get(i + 1))) {
					Collections.swap(listeBateau, i, i + 1);
					trie = false;
				}
			}

			if (trie)
				trier = false;
		}
	}
}
