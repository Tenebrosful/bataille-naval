package jeu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import exception.CaseInexistanteException;
import exception.CaseOccupeeException;
import exception.ListeBateauVideException;
import exception.TailleException;

/**
 * Mod�lisation de la grille de jeu
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class Grille implements Serializable{

	/**
	 * Constructeur de Grille.java par d�faut
	 */
	public Grille() {
		this.tailleX = 20;
		this.tailleY = 20;
		
		initialiserCases();
	}

	/**
	 * Retourne grille
	 * @return grille
	 */
	public List<Case> getGrille() {
		return grille;
	}

	/**
	 * Constructeur de Grille.java pour une dimension (x;y) donn�e
	 * 
	 * @param tailleX Taille en x de la grille
	 * @param tailleY Taille en y de la grille
	 * @throws TailleException Lanc�e lorsque la taille en x ou y est n�gative
	 */
	public Grille(int tailleX, int tailleY) throws TailleException {

		if (tailleX <= 0)
			throw new TailleException("La taille en X ne peut pas �tre n�gative (" + tailleX + ")");
		if (tailleY <= 0)
			throw new TailleException("La taille en X ne peut pas �tre n�gative (" + tailleX + ")");

		this.tailleX = tailleX;
		this.tailleY = tailleY;

		initialiserCases();

	}
	
	private void initialiserCases() {
		this.grille = new ArrayList<Case>();
		for (int i = 0; i < tailleY; ++i) {
			for (int j = 0; j < tailleX; ++j) {
				grille.add(new Case(j, i));
			}
		}
	}

	/**
	 * Taille en x de la grille
	 */
	private int tailleX;

	/**
	 * Taille en y de la grille
	 */
	private int tailleY;

	/**
	 * Liste des cases de la grille
	 */
	private List<Case> grille;

	/**
	 * Permet d'afficher la grille du point de vue d'un adversaire (bateaux
	 * apparents) V = Bateau touch� X = Case touch�e sans bateau
	 * 
	 * @return L'affichage de la grille du point de vue d'un adversaire
	 */
	public String afficherAdversaire() {
		StringBuilder builder = new StringBuilder();
		int y = 0;
		for (Case tmp : this.grille) {
			if (y != tmp.getPosY()) {
				y = tmp.getPosY();
				builder.append("\n");
			}

			builder.append(tmp.afficherAdversaire());
		}
		return builder.toString();
	}

	/**
	 * Permet d'afficher la grille du point de vue de son propri�taire (bateaux
	 * apparents) O = Bateau touch� B = Bateau
	 * 
	 * @return L'affichage de la grille du point de vue de son propri�taire
	 */
	public String afficherProprietaire() {
		StringBuilder builder = new StringBuilder();
		int y = 0;
		for (Case tmp : this.grille) {
			if (y != tmp.getPosY()) {
				y = tmp.getPosY();
				builder.append("\n");
			}

			builder.append(tmp.afficherProprietaire());
		}
		return builder.toString();
	}

	/**
	 * Teste s'il reste un bateau non coul� dans la grille
	 * 
	 * @return True s'il reste au moins une case avec un bateau non-coul�
	 */
	public boolean avoirBateauVivant() {
		for (Case tmp : this.grille) {
			if (tmp.isOccupee() && !tmp.isImpact()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Renvoi la liste des cases de la grille o� se trouve un bateau donn�
	 * 
	 * @param bateau Bateau dont on va renvoyer la liste de case
	 * @return Liste des cases o� le bateau est pr�sent
	 * @throws ListeBateauVideException Lanc�e lorsque le bateau n'est pr�sent sur
	 *                                  aucune case de la grille
	 */
	public List<Case> casesBateau(Bateau bateau) throws ListeBateauVideException {
		List<Case> res = new ArrayList<Case>();

		for (Case tmp : this.grille) {
			if (tmp.getBateau() != null && tmp.getBateau().equals(bateau)) {
				res.add(tmp);
			}
			if (res.size() >= bateau.getLongueur()) {
				break;
			}
		}

		if (res.isEmpty())
			throw new ListeBateauVideException("Le bateau n'existe pas sur cette grille");

		return res;
	}

	/**
	 * Retourne le nombre de case touch�e d'un bateau donn� sur la grille
	 * 
	 * @param bateau Bateau dont le nombre de case touch�e sera retourn�e
	 * @return Le nombre de case du bateau touch�es, retourne -1 si le bateau n'est
	 *         pas sur la grille
	 */
	public int etatBateau(Bateau bateau) {
		int res = 0;

		try {
			for (Case tmp : this.casesBateau(bateau)) {
				if (tmp.isImpact())
					res++;
			}
		} catch (ListeBateauVideException e) {
			return -1;
		}

		return res;
	}

	/**
	 * Retourne la case correspondant � la position (x;y) donn�
	 * 
	 * @param posX Position en x de la case recherch�e
	 * @param posY Position en y de la case recherch�e
	 * @return Case correspondante aux coordonn�es
	 * @throws CaseInexistanteException Lanc�e lorsque il n'existe pas de case � la
	 *                                  position (x;y) dans la grille
	 */
	public Case getCase(int posX, int posY) throws CaseInexistanteException {
		if (this.positionValide(posX, posY)) {
			for (Case tmp : this.grille) {
				if (tmp.aPourCoordonnee(posX, posY))
					return tmp;
			}
		} else {
			throw new CaseInexistanteException("La case (" + posX + ";" + posY + ") n'existe pas");
		}
		return null;
	}

	/**
	 * Retourne tailleX
	 * @return tailleX
	 */
	public int getTailleX() {
		return tailleX;
	}

	/**
	 * Retourne tailleY
	 * @return tailleY
	 */
	public int getTailleY() {
		return tailleY;
	}

	/**
	 * Teste si la position (x;y) est valide sur la grille
	 * 
	 * @param posX Position en x � tester
	 * @param posY Position en y � tester
	 * @return True si la position (x;y) est valide
	 */
	public boolean positionValide(int posX, int posY) {
		return !(posX < 0 || posY < 0 || posX >= this.tailleX || posY >= this.tailleY);
	}

	/**
	 * Attribu un bateau � une case avec une position (x,y) de la grille
	 * 
	 * @param bateau Bateau � attribuer
	 * @param posX   Position en x de la case
	 * @param posY   Position en y de la case
	 * @throws CaseInexistanteException Lanc�e lorsque la case en (x;y) n'existe pas
	 * @throws CaseOccupeeException     Lanc�e lorsque la case en (x;y) est d�j�
	 *                                  occup�e par un bateau
	 */
	public void setBateauCase(Bateau bateau, int posX, int posY) throws CaseInexistanteException, CaseOccupeeException {
		if (this.positionValide(posX, posY)) {
			Case tmp = this.getCase(posX, posY);
			int indexTmp = this.grille.indexOf(tmp);
			if (tmp.getBateau() == null) {
				tmp.setBateau(bateau);
				this.grille.set(indexTmp, tmp);
			} else {
				throw new CaseOccupeeException(
						"La case (" + tmp.getPosX() + ";" + tmp.getPosY() + ") poss�de d�j� un bateau");
			}
		} else {
			throw new CaseInexistanteException("La case (" + posX + ";" + posY + ") n'existe pas");
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Grille [tailleX=");
		builder.append(tailleX);
		builder.append(", tailleY=");
		builder.append(tailleY);
		builder.append("]\n");
		int y = 0;
		for (Case tmp : this.grille) {
			if (y != tmp.getPosY()) {
				y = tmp.getPosY();
				builder.append("\n");
			}

			builder.append(tmp);
		}
		return builder.toString();
	}
}
