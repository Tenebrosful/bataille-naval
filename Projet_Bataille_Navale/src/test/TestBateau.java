/**
 * 
 */
package test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import exception.LongueurException;
import jeu.Bateau;

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class TestBateau {

	static Bateau b;
	static Bateau b1;
	static Bateau b2;
	static Bateau b3;

	/**
	 * Teste l'ajout et le retrait d'un nom de bateau dans la liste par d�faut
	 */
	@Test
	public void ajouterRetirerNom() {

		Bateau.addListeNomBateauDefaut("Oui");

		assertEquals("Le nom aurait d�t �tre ajout�", true, Bateau.getListeNomBateauDefaut().contains("Oui"));

		Bateau.removeListeNomBateauDefaut("Oui");

		assertEquals("Le nom aurait d�t �tre retir�", false, Bateau.getListeNomBateauDefaut().contains("Oui"));
	}

	/**
	 * Teste la comparaison de 2 bateaux
	 * 
	 * @throws LongueurException Lanc�e si le bateau a une longueur incorrecte
	 */
	@Test
	public void comparaison() throws LongueurException {

		Bateau b1 = new Bateau(2, "");
		Bateau b2 = new Bateau(1, "");
		Bateau b3 = new Bateau(3, "");
		Bateau b4 = new Bateau(2, "");

		assertEquals("La fonction devrait renvoyer 0", 0, b1.compareTo(b4));
		assertEquals("La fonction devrait renvoyer -1", -1, b1.compareTo(b2));
		assertEquals("La fonction devrait renvoyer 1", 1, b1.compareTo(b3));
	}

	/**
	 * Test d'un constructeur avec une longueur n�gative
	 * 
	 * @throws LongueurException Lanc�e si la longueur du bateau est n�gative
	 */
	@Test(expected = LongueurException.class)
	public void constructeurLongueurNegative() throws LongueurException {
		new Bateau(-1, null);
	}

	/**
	 * Test d'un constructeur avec une longueur n�gative
	 * 
	 * @throws LongueurException Lanc�e si la longueur du bateau est n�gative
	 */
	@Test(expected = LongueurException.class)
	public void constructeurLongueurNulle() throws LongueurException {
		new Bateau(0, null);
	}

	/**
	 * Test d'un constructeur pour un nom vide ou null
	 * 
	 * @throws LongueurException Lanc�e si la longueur du bateau est n�gative
	 */
	@Test
	public void constructeurNomVide() throws LongueurException {
		b1 = new Bateau(2, "");
		b2 = new Bateau(2, null);

		assertEquals("Le nom ne devrait pas �tre vide", false, b1.getNom().isEmpty());
		assertEquals("Le nom ne devrait pas �tre null", false, b2.getNom().isEmpty());
	}

	/**
	 * Test d'un constructeur execut� normalement
	 * 
	 * @throws LongueurException Lanc�e si la longueur du bateau est n�gative
	 */
	@Test
	public void constructeurOK() throws LongueurException {
		b = new Bateau(2, "Charle de Gaulle");

		assertEquals(2, b.getLongueur());
		assertEquals("Charle de Gaulle", b.getNom());
		assertEquals(0, b.getPosX());
		assertEquals(0, b.getPosY());
		assertEquals(null, b.getOrientation());
	}

	/**
	 * Teste la d�finition de la liste de nom par d�faut
	 */
	@Test
	public void definirListeNom() {
		ArrayList<String> lNom = new ArrayList<String>();

		lNom.add("Coucou");
		lNom.add("Ca va ?");
		lNom.add("Bah nickel");
		Bateau.setListeNomBateauDefaut(lNom);

		assertEquals("La liste aurait du �tre remplac�e", true, Bateau.getListeNomBateauDefaut().equals(lNom));
	}

	/**
	 * Teste la d�finition de la position avec la m�thode setPos(int,int)
	 * 
	 * @throws LongueurException Lanc�e si le bateau a une longueur incorrecte
	 */
	@Test
	public void definirPosition() throws LongueurException {
		Bateau b1 = new Bateau(2, "");

		b1.setPos(5, 6);

		assertEquals("La position en x devrait �tre 5", 5, b1.getPosX());
		assertEquals("La position en x devrait �tre 6", 6, b1.getPosY());
	}

	/**
	 * Teste la m�thode equals
	 * 
	 * @throws LongueurException Lanc�e si la longueur est �rron�e
	 */
	@Test
	public void equals() throws LongueurException {
		Bateau b1 = new Bateau(2, "BeauBateau");
		Bateau b2 = new Bateau(2, "BeauBateau");
		Bateau b3 = new Bateau(3, "BeauBateau");
		Bateau b4 = new Bateau(2, "MoinsBeauBateau");
		Bateau b5 = new Bateau(2, "BeauBateau");
		Bateau b6 = new Bateau(2, "BeauBateau");
		Bateau b7 = new Bateau(2, "BeauBateau");
		Bateau b8 = new Bateau(2, "BeauBateau");

		b5.setOrientation("Droite");
		b8.setOrientation("Gauche");
		b6.setPosX(2);
		b7.setPosY(2);

		assertEquals("La fonction devrait renvoyer true", true, b1.equals(b1));
		assertEquals("La fonction devrait renvoyer true", true, b1.equals(b2));
		assertEquals("La fonction devrait renvoyer false", false, b1.equals(b3));
		assertEquals("La fonction devrait renvoyer false", false, b1.equals(b4));
		assertEquals("La fonction devrait renvoyer false", false, b1.equals(null));
		assertEquals("La fonction devrait renvoyer false", false, b1.equals(new Object()));
		assertEquals("La fonction devrait renvoyer false", false, b1.equals(b5));
		assertEquals("La fonction devrait renvoyer false", false, b1.equals(b6));
		assertEquals("La fonction devrait renvoyer false", false, b1.equals(b7));
		assertEquals("La fonction devrait renvoyer false", false, b5.equals(b8));
	}

	/**
	 * Teste le toString
	 * 
	 * @throws LongueurException Lanc�e si le bateau a une longueur incorrecte
	 */
	@Test
	public void ToString() throws LongueurException {
		Bateau b1 = new Bateau(2, "Oui");

		String res = "Oui de longueur 2";

		assertEquals("La fonction ne revoit pas le bon String", res, b1.toString());
	}

}
