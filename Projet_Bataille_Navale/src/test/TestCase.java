/**
 * 
 */
package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import exception.LongueurException;
import jeu.Bateau;
import jeu.Case;

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class TestCase {

	/**
	 * Teste le constructeur
	 */
	@Test
	public void constructeur() {
		Case c = new Case(0, 0);

		assertEquals("La position en x devrait �tre 0", 0, c.getPosX());
		assertEquals("La position en y devrait �tre 0", 0, c.getPosY());
	}

	/**
	 * Teste l'affichage du point de vu d'un adversaire
	 * 
	 * @throws LongueurException Lanc�e lorsque la longueur est �rron�e
	 */
	@Test
	public void affichageAdversaire() throws LongueurException {
		Case c1 = new Case(0, 0);
		Case c2 = new Case(0, 1);

		c2.setBateau(new Bateau(2, null));

		assertEquals("Le string renvoy� est incorrect", "[ ]", c1.afficherAdversaire());
		assertEquals("Le string renvoy� est incorrect", "[ ]", c2.afficherAdversaire());

		c1.setImpact(true);
		c2.setImpact(true);

		assertEquals("Le string renvoy� est incorrect", "[X]", c1.afficherAdversaire());
		assertEquals("Le string renvoy� est incorrect", "[V]", c2.afficherAdversaire());
	}

	/**
	 * Teste l'affichage du point de vu du propri�taire
	 * 
	 * @throws LongueurException Lanc�e lorsque la longueur est �rron�e
	 */
	@Test
	public void affichageProprietaire() throws LongueurException {
		Case c1 = new Case(0, 0);
		Case c2 = new Case(0, 1);

		c2.setBateau(new Bateau(2, null));

		assertEquals("Le string renvoy� est incorrect", "[ ]", c1.afficherProprietaire());
		assertEquals("Le string renvoy� est incorrect", "[B]", c2.afficherProprietaire());

		c1.setImpact(true);
		c2.setImpact(true);

		assertEquals("Le string renvoy� est incorrect", "[ ]", c1.afficherProprietaire());
		assertEquals("Le string renvoy� est incorrect", "[O]", c2.afficherProprietaire());
	}

	/**
	 * Teste la m�thode aPourCoordonnee
	 */
	@Test
	public void aPourCoordonnee() {
		Case c = new Case(1, 2);

		assertEquals("La fonction devrait renvoyer true", true, c.aPourCoordonnee(1, 2));
		assertEquals("La fonction devrait renvoyer false", false, c.aPourCoordonnee(1, 3));
		assertEquals("La fonction devrait renvoyer false", false, c.aPourCoordonnee(2, 2));
		assertEquals("La fonction devrait renvoyer false", false, c.aPourCoordonnee(0, 0));
	}

	/**
	 * Teste le getteur pour le bateau
	 * 
	 * @throws LongueurException Lanc�e lorsque la longueur est erron�e
	 */
	@Test
	public void getBateau() throws LongueurException {
		Case c = new Case(0, 0);
		Bateau b = new Bateau(2, null);

		c.setBateau(b);

		assertEquals("Le bateau renvoy� devrait �tre le m�me que celui de la case", b, c.getBateau());
	}

	/**
	 * 	Teste la m�thode toString
	 */
	@Test
	public void ToString() {
		Case c = new Case(0, 0);
		
		String res = "[0;0;false;false]";
		
		assertEquals("Le string renvoy� est incorrect",res,c.toString());
	}

}
