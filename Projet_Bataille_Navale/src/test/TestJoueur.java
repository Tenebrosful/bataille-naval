/**
 * 
 */
package test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import exception.ListeBateauVideException;
import exception.LongueurException;
import jeu.Bateau;
import jeu.Grille;
import jeu.Joueur;

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class TestJoueur {
	
	static ArrayList<Bateau> lb = new ArrayList<Bateau>();
	static Grille g = new Grille();
	
	/**
	 * Execut� avant chaque test
	 * @throws LongueurException Lanc�e losque la longueur est �rron�e
	 */
	@Before
	public void setup() throws LongueurException {
		
		 lb.add(new Bateau(2,null));
		 lb.add(new Bateau(3,null));
		 lb.add(new Bateau(4,null));		 
	}

	/**
	 * Teste le constructeur de Joueur
	 * @throws ListeBateauVideException Lanc�e losque la longueur est �rron�e
	 */
	@Test
	public void constructeur() throws ListeBateauVideException {
		Joueur j1 = new Joueur("Jacques", g, lb);
		
		assertEquals("Le nom devrait �tre Jacques","Jacques",j1.getNomJoueur());
		assertEquals("La grille devrait �tre celle plac�e en param�tre",g,j1.getGrille());
		assertEquals("La liste de bateau devrait �tre celle plac�e en param�tre",lb,j1.getListeBateau());
	}

}
