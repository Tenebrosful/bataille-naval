/**
 * 
 */
package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.*;

import exception.CaseInexistanteException;
import exception.CaseOccupeeException;
import exception.ListeBateauVideException;
import exception.LongueurException;
import exception.TailleException;
import jeu.Bateau;
import jeu.Case;
import jeu.Grille;

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class TestGrille {

	/**
	 * Teste le constructeur vide
	 */
	@Test
	public void constructeurVide() {
		Grille g = new Grille();

		assertEquals("La taille en x de la grille devrait �tre de 20", 20, g.getTailleX());
		assertEquals("La taille en y de la grille devrait �tre de 20", 20, g.getTailleY());

		assertEquals("La taille de la liste de case devrait �tre de 400", 400, g.getGrille().size());

		// Pas de v�rification du contenu de la liste actuellement
	}

	/**
	 * Teste le constructeur avec la taille de la grille
	 * 
	 * @throws TailleException Lanc�e lorsque la taille est �rron�e
	 */
	@Test
	public void constructeurOK() throws TailleException {
		Grille g = new Grille(40, 10);

		assertEquals("La taille en x de la grille devrait �tre de 40", 40, g.getTailleX());
		assertEquals("La taille en y de la grille devrait �tre de 10", 10, g.getTailleY());

		assertEquals("La taille de la liste de case devrait �tre de 400", 400, g.getGrille().size());

		// Pas de v�rification du contenu de la liste actuellement
	}

	/**
	 * Teste le constructeur lorsque la taille en x est erron�
	 * 
	 * @throws TailleException Lanc�e lorsque la taille est �rron�e
	 */
	@Test(expected = TailleException.class)
	public void constructeurMauvaiseTailleX() throws TailleException {
		new Grille(-2, 10);
	}

	/**
	 * Teste le constructeur lorsque la taille en y est erron�
	 * 
	 * @throws TailleException Lanc�e lorsque la taille est �rron�e
	 */
	@Test(expected = TailleException.class)
	public void constructeurMauvaiseTailleY() throws TailleException {
		new Grille(2, -2);
	}

	/**
	 * Teste l'affichage de la grille du point de vu d'un adversaire
	 * 
	 * @throws TailleException Lanc�e lorsque la taille est �rron�e
	 */
	@Test
	public void affichageAdversaire() throws TailleException {
		Grille g = new Grille(2, 2);

		assertEquals("Le string renvoy� est incorrect", "[ ][ ]\n[ ][ ]", g.afficherAdversaire());
	}

	/**
	 * Teste l'affichage de la grille du point de vu d'un adversaire
	 * 
	 * @throws TailleException Lanc�e lorsque la taille est �rron�e
	 */
	@Test
	public void affichageProprietaire() throws TailleException {
		Grille g = new Grille(2, 2);

		assertEquals("Le string renvoy� est incorrect", "[ ][ ]\n[ ][ ]", g.afficherProprietaire());
	}

	/**
	 * Teste la m�thode avoir Bateau Vivant
	 * 
	 * @throws LongueurException        Lanc�e lorsque la longueur est �rron�e
	 * @throws CaseOccupeeException     Lanc�e lorsque la case est d�j� occup�e
	 * @throws CaseInexistanteException Lanc�e lorsque la case n'existe pas
	 */
	@Test
	public void avoirBateauVivant() throws CaseInexistanteException, CaseOccupeeException, LongueurException {
		Grille g = new Grille();

		g.setBateauCase(new Bateau(2, null), 0, 0);

		assertEquals("La fonction devrait retourner true", true, g.avoirBateauVivant());

		g.getCase(0, 0).setImpact(true);

		assertEquals("La fcontion devrait retourner false", false, g.avoirBateauVivant());
	}

	/**
	 * Teste la m�thode getCase() lorsque la case n'existe pas
	 * 
	 * @throws CaseInexistanteException Lanc�e lorsque la case n'existe pas
	 */
	@Test(expected = CaseInexistanteException.class)
	public void getCase() throws CaseInexistanteException {
		Grille g = new Grille();

		g.getCase(30, 30);
	}

	/**
	 * Teste la m�thode casesBateau
	 * 
	 * @throws LongueurException        Lanc�e lorsque la longueur est �rron�e
	 * @throws CaseOccupeeException     Lanc�e lorsque la case est d�j� occup�e
	 * @throws CaseInexistanteException Lanc�e lorsque la case n'existe pas
	 * @throws ListeBateauVideException Lanc�e lorsque la liste de bateau est vide
	 */
	@Test
	public void casesBateau()
			throws LongueurException, CaseInexistanteException, CaseOccupeeException, ListeBateauVideException {
		Grille g = new Grille();
		Bateau b = new Bateau(2, null);
		ArrayList<Case> l = new ArrayList<Case>();

		l.add(g.getCase(0, 0));
		l.add(g.getCase(0, 1));
		g.setBateauCase(b, 0, 0);
		g.setBateauCase(b, 0, 1);

		assertEquals("La liste retourn�e devrait correspondre", l, g.casesBateau(b));
	}

	/**
	 * Teste la m�thode casesBateau lorsque le bateau n'est pas sur la grille
	 * 
	 * @throws LongueurException        Lanc�e lorsque la longueur est �rron�e
	 * @throws ListeBateauVideException Lanc�e lorsque la liste de bateau est vide
	 */
	@Test(expected = ListeBateauVideException.class)
	public void casesBateauNull() throws LongueurException, ListeBateauVideException {
		Grille g = new Grille();
		g.casesBateau(new Bateau(2, null));
	}

	/**
	 * Teste la m�thode etatBateau
	 * 
	 * @throws LongueurException        Lanc�e lorsque la longueur est �rron�e
	 * @throws CaseOccupeeException     Lanc�e lorsque la case est d�j� occup�e
	 * @throws CaseInexistanteException Lanc�e lorsque la case n'existe pas
	 */
	@Test
	public void etatBateau() throws LongueurException, CaseInexistanteException, CaseOccupeeException {
		Grille g = new Grille();
		Bateau b = new Bateau(2, null);
		
		assertEquals("La fonction devrait retourner -1",-1,g.etatBateau(new Bateau(2, null)));

		g.setBateauCase(b, 0, 0);
		g.setBateauCase(b, 0, 1);

		assertEquals("La m�thode devrait retourner 0", 0, g.etatBateau(b));

		g.getCase(0, 0).setImpact(true);

		assertEquals("La m�thode devrait retourner 1", 1, g.etatBateau(b));

		g.getCase(0, 1).setImpact(true);
		
		assertEquals("La m�thode devrait retourner 2", 2, g.etatBateau(b));

	}
	
	/**
	 * Teste la m�thode toString()
	 * @throws TailleException Lanc�e lorsque la longueur est �rron�e
	 */
	@Test
	public void ToString() throws TailleException {
		Grille g = new Grille(2,2);
		
		String res = "Grille [tailleX=2, tailleY=2]\n" + "[0;0;false;false][1;0;false;false]\n" + "[0;1;false;false][1;1;false;false]";
		
		assertEquals("Le String retourn� est incorrect", res, g.toString());
	}
}
